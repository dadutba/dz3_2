#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Реализация json api Яндекс переводчика

"""
import chardet
import os
import requests


#API_KEY = 'trnsl.1.1.20161025T233221Z.47834a66fd7895d0.a95fd4bfde5c1794fa433453956bd261eae80152'
API_KEY = 'trnsl.1.1.20180725T113357Z.29b033ee476eb4cc.fde75e401beb28e78c0809c2327e2cd569e3aefa'
URL_BASE = 'https://translate.yandex.net/api/v1.5/tr.json'
TRANSLATE_URL = f'{URL_BASE}/translate'
GET_LANGS_URL = f'{URL_BASE}/getLangs'
test_input = 'Some very bad text'
test_file = 'ES.txt'
default_extension = '.txt' # Расширение переводимых файлов
default_suffix = '_trans' # Суффикс для переведенных файлов
copyright_string = '\nПереведено сервисом «Яндекс.Переводчик»'\
                   '\nhttp://translate.yandex.ru/\n'

script_dir = os.path.dirname(os.path.abspath(__file__))
test_file_full = os.path.join(script_dir, test_file)

def get_language_list(ui_lang='ru', get_request=False):
    """
    https://translate.yandex.net/api/v1.5/tr.json/getLangs
     ? [key=<API-ключ>]
     & [ui=<код языка>]
     & [callback=<имя callback-функции>]
     
     Args:
        ui_lang:      язык, на котором выводить описания языков
        get_request:  флаг использовать ли get запрос        
    
    Returns:
        Словарь с информацией код языка - описание
        
    """
    params = {
        'key': API_KEY,
        'ui': ui_lang
    }
    
    if get_request:
        response = requests.get(GET_LANGS_URL, params=params)
    else:
        response = requests.post(GET_LANGS_URL, data=params)
        
    json_ = response.json()

    if not 'langs' in json_:
        raise requests.RequestException(
                'Error while getting available languages descriptions')
    
    return json_['langs']


def translate_it(text,
                 from_lang=None,
                 to_lang='ru',
                 get_request=False,
                 format='plain'):
    """
    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    Args:
        text:         текст, который нужно перевести
        from_lang:    код языка для переводимого текста, None - автоопределение
        to_lang:      на какой язык переводить
        get_request:  флаг использовать ли get запрос
        format:       формат исходного текста ('plain' или 'html')
        
    
    Returns:
        Переведенная строка
        
    """
    
    if from_lang is None:
        lang = to_lang
    else:
        lang = ''.join([from_lang, '-', to_lang])
    
    params = {
        'key': API_KEY,
        'text': text,
        'lang': lang,
        'format': format
    }

    if get_request:
        response = requests.get(TRANSLATE_URL, params=params)
    else:
        response = requests.post(TRANSLATE_URL, params=params)
        
    json_ = response.json()
    rezult = ''

    if json_['code'] == 200:
        rezult = ''.join(json_['text'])
    else:
        raise requests.RequestException(
                f'Error: {json_["code"]}, {json_["message"]}')   
    # print(json_)
    rezult += copyright_string
    return rezult


def translate_file(in_file,
                   out_file=None,
                   from_lang=None,
                   to_lang='ru',
                   get_request=False,
                   format='plain'):
    """
    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    Args:
        in_file:      путь до входного файла, текст которого нужно перевести
        out_file:     путь до выходного файла. Если None печать на экран
        from_lang:    код языка для переводимого текста, None - автоопределение
        to_lang:      на какой язык переводить
        get_request:  флаг использовать ли get запрос
        format:       формат исходного текста ('plain' или 'html')
        
    
    Returns:
        None
        
    """
    
    with open(in_file, 'br') as file:
        enc = chardet.detect(file.read(512))['encoding']
            
    with open(in_file, encoding=enc, errors='ignore') as file:
        text = file.read()
        
    text = translate_it(text, from_lang, to_lang, get_request, format)
    
    if out_file is None:
        print(text)
    else:
        with open(out_file, 'wt', encoding=enc, errors='ignore') as file:
            file.write(text)


def main():
    print(f'Исходный текст: {test_input}')
    print(f'Перевод: {translate_it(test_input)}')
    
    print(f'Перевод файла {test_file} на экран...')
    translate_file(test_file_full)
    
    print('Перевод всех файлов в папке...')
    files = [file for file in os.listdir(script_dir)
             if file.lower().endswith(default_extension)
             and not file.lower().endswith(default_suffix + default_extension)]
    for file in files:
        print(f'\tПеревод файла {file}...')
        filename, file_extension = os.path.splitext(file)
        out_file = ''.join([filename, default_suffix, file_extension])
        translate_file(os.path.join(script_dir, file),
                       os.path.join(script_dir, out_file))
    print(f'\nГотово. Переведено {len(files)} файлов')
    
    print('Получение списка поддерживаемых языков:')
    langs = get_language_list()
    for code, description in langs.items():
        print(f'\t{code}\t{description}')


if __name__ == '__main__':
    main()